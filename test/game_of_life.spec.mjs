import chai from 'chai'
chai.should()

const getNeighbourNumber = (state, cell) => {
	return state
		.filter(c => c !== cell)
		.filter(({x}) => isNeighbour(x, cell))
		.filter(({y}) => isNeighbour(y, cell))
		.length
}

const isNeighbour = (position, cell) => position === (cell.y + 1) || position === (cell.y - 1) || position === cell.y

const gameTick = state => state.filter(cell => getNeighbourNumber(state, cell) >= 2)


describe('Game of life', () => {
	describe('Calculate neighbours', () => {
		it('should give neighbours number', () => {
			const state = [
				{ x: 0, y: 0 },
			]
			getNeighbourNumber(state, state[0]).should.be.equal(0)
		});

		it('should give neighbours number', () => {
			const state = [
				{ x: 0, y: 0 },
				{ x: 0, y: 1 },
			]
			getNeighbourNumber(state, state[0]).should.be.equal(1)
		});

		it('should give neighbours number', () => {
			const state = [
				{ x: 0, y: 0 },
				{ x: 0, y: 2 },
			]
			getNeighbourNumber(state, state[0]).should.be.equal(0)
		});
	})

	describe('give the next tick', () => {
		describe('cells should die when it have less than two neighbours', () => {
			it('case : 1 living cell', () => {
				const state = [
					{ x: 0, y: 0 },
				]
				gameTick(state).should.be.an('array').empty;
			});

			it('case : 3 living cells', () => {
				const state = [
					{ x: 0, y: 0 },
					{ x: 0, y: 1 },
					{ x: 0, y: -1 },
				]
				gameTick(state).should.be.deep.equal([
					{ x: 0, y: 0 },
				]);
			});
		});

		describe('cell should stay alive if it have 2 or 3 neighbours', () => {
			it('case : 4 living cells', () => {
				const state = [
					{ x: 0, y: 0 },
					{ x: 0, y: 1 },
					{ x: 1, y: 0 },
					{ x: 1, y: 1 },
				]
				gameTick(state).should.be.deep.equal([
					{ x: 0, y: 0 },
					{ x: 0, y: 1 },
					{ x: 1, y: 0 },
					{ x: 1, y: 1 },
				]);
			});
		});
	});

});
